<?php include 'inc/header.php';?>

<?php
    $login = Session::get("usrlogin");
    if($login == false){
        header("Location:login.php");
    }
 ?>
  <?php 
 	 if(isset($_GET['userid'])){
 		$id   = $_GET['userid'];
 		$time = $_GET['time'];
 		$remove = $add->addRemoveFromAllAd($id,$time);
 	}
   ?>

<style>
 .settingSidebar{
		min-height: 70%;
		padding: 0 0 0 35px;
		margin-left: 10px;
		margin-top: 18px;
		font-family: 'Vollkorn', serif;
		font-size: 20px;
		font-weight: 800px;
		float: left;
 }
 .mainbar{
 	width: 60%;
 	min-height: 70%;
 	/*border: 1px solid red;*/
 	margin: 0 0 0 25%;
 }

 table {
     border-collapse: collapse;
     width: 100%;
 }

 th, td {
     text-align: left;
     padding: 15px;
 }

 tr:nth-child(even){background-color: #f2f2f2}

 th {
     background-color: #4CAF50;
     color: white;
 }
 #message{
 	color:red;
 	font-weight: 800;
 }
 .myaccount a {
 	color: black;
 	font-weight: 800;
 }
 .myaccount a:hover {
    color: #88a9e0;
}


 .myaccount a:active {
    color: blue;
}
</style>




<br>
<div class="settingSidebar">
	<br><br><br>
	&#8618;<a href="myaccount.php" title="">My Account</a><br><br>
	&#8618;<a href="settings.php" title="">Settings</a>
</div>

<div class="mainbar">


<?php
  $usrId = Session::get("usrId");
  $getHistory = $add->getAdHistory($usrId);
	
 if($getHistory){ ?>
	<h1 class="myaccountTitle" align="center">This is your add history.</h1> <br>
	<table>
	  <tr>
	    <th width="3%">SL</th>
	    <th width="20%">Ad Title</th>
	    <th width="8%">Image</th>
	    <th width="10%">Post Date</th>
	    <th width="8%">Action</th>
	  </tr>

	<?php
	$i=0;
	while($result = $getHistory->fetch_assoc()){
			$i++;
?>
       	
	  <tr>
	    <td> <?php echo $i;?></td>
	    <td><?php echo $result['caption']; ?></td>
	    <td><img src="<?php echo $result['image'];?>" alt=""  width="80" height="40" /></td>
	    <td><?php echo $fm->formatDate($result['date']); ?></td>
	    <td class="myaccount">
	    	<?php
	    	if($result['type'] == '0') { ?>

	    	<a href="editpost.php?postid=<?php echo $result['postId']; ?>">
	    	Edit</a> || <a onclick="return confirm('Are you sure you want to remove this ad?')" 
	    	href="?userid=<?php echo $usrId; ?>  &time=<?php echo $result['date'];?>">Remove</a>

	    <?php	}else{
	    		echo '<div id="message">Removed</div>';
	    	} ?>
	    </td>
	    <?php }?>
	  </tr>
		<?php   } else { ?>
		<h1 class="myaccountTitle" align="center" style="margin-top: 15%;">Your Ad history will be shown here. <br> You haven't create any post yet.
 		<div class="allAdDivSubmit" style="margin-left: 32%">
		<a href="offerforrent.php" title="">Post Your First Ad</a></div></h1> 

		<?php } ?>
	</table>

</div>
<br><br>

<?php include 'inc/footer.php';?>
