

<!-- +++++++++++++++++++++++++++++++++++++++++++++++
    AUTOMATIC SLIDESHOW 
++++++++++++++++++++++++++++++++++++++++++++++++++++-->


<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 3</div>
  <img src="images/slide1.png" style="width:100%">
  <div class="text"><b><a href="#" style="color:yellow;" title="">Rent House Up to 30% Off</a></b></div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 3</div>
  <img src="images/slide2.png" style="width:100%">
   <div class="text"><b><a href="#" style="color:yellow;"  title="">Rent House Up to 30% Off</a></b></div>
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 3</div>
  <img src="images/slide3.png" style="width:100%">
   <div class="text"><b><a href="#" style="color:yellow;" title="">Rent House Up to 30% Off</a></b></div>
   
</div>

<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>

<br><br><br><br>

<script type="text/javascript" src="js/slideshow.js"></script>
<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light+Two" rel="stylesheet">

