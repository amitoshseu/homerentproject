<!-- +++++++++++++++++++++++++++++++++++++++++++++++
    FOOTER PART
++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<link href="css/footerStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script> 



   <div class="footer">
      <div class="wrapper"> 
       <div class="section group">
        <div class="col_1_of_4 span_1_of_4">
            <h4>Information</h4>
            <ul>
            <li><a href="#">Services</a></li>
            <li><a href="#">Agent</a></li>
            <li><a href="#">Block</a></li>
            <li><a href="contact.php"><span>Contact Us</span></a></li>
            </ul>
          </div>
        <div class="col_1_of_4 span_1_of_4">
          <h4>Why rent from us</h4>
            <ul>
            <li><a href="about.php">About Us</a></li>
            <li><a href="service.php">Customer Service</a></li>
            <li><a href="contact.php">Contact Us</a></li>
            <li><a href="contact.php#map"><span>Site Map</span></a></li>
            </ul>
        </div>
        <div class="col_1_of_4 span_1_of_4">
          <h4>My account</h4>
            <ul>
              <li><a href="login.php">Sign In</a></li>
              <li><a href="register.php">Sign Up</a></li>
              <li><a href="offerforrent.php">Post Add</a></li>
              <li><a href="myaccount.php"><span>Ad History History</span></a></li>
            </ul>
        </div>
        <div class="col_1_of_4 span_1_of_4">
          <h4>Contact</h4>
            <ul>
              <li><span>+8801636502457</span></li>
              <li><span>+8801764104166</span></li>
            </ul>
            <div class="social-icons">
              <h4>Follow Us</h4>
                  <ul>
                    <li class="facebook"><a href="http://www.facebook.com/arif.rabbani.102"> </a></li>
                    <li class="twitter"><a href="http://www.twitter.com/arif.rabbani.102"> </a></li>
                    <li class="googleplus"><a href="https://plus.google.com/+arif.rabbani.102"> </a></li>
                    <li class="contact"><a href="#" target="_blank"> </a></li>
                    <div class="clear"></div>
                 </ul>
              </div>
        </div>
      </div>
      <div class="copy_right">
        <p>
         &copy; Copyright <a href="about.php#developerTeam">House Rent Research Team</a>. All Rights Reserved.
        </p>

       </div>
     </div>
    </div>
    
    <script type="text/javascript">
    $(document).ready(function() {
     
       $().UItoTop({ easingType: 'easeOutQuart' });
      
    });
  </script>
    <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
</body>
</html>
  
