<link href="https://fonts.googleapis.com/css?family=Fresca" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Freckle+Face" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet">
<?php 
    include 'lib/Session.php'; 
    Session::init();
    include 'lib/Database.php'; 
    include 'helpers/Format.php'; 

    spl_autoload_register(function($class){
      include_once "classes/".$class.".php";

    });

    $db   = new Database();
    $fm   = new Format();
    $usr  = new Users();
    $mess = new Message();
    $add  = new Post_Add(); 
 ?>
<?php
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache"); 
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
  header("Cache-Control: max-age=2592000");
?>

<!DOCTYPE html>
<html>
<head>
<meta content="text/html;"/>
<title>House Rent</title>



<link rel="icon" type="image/png" href="titleCar.png" />
<link href="css/styles.css" rel="stylesheet" type="text/css" />

</head>
<body>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++
    NAME and LOGO PART
++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


<div class="header">
  <div class="logo">
    
  <a href="index.php" title=""><h1>House<span>Rent</span></h1></a>
  </div>

  <?php 
    if(isset($_GET['cid'])){
      // $delData = $res->delCustomerReservation();
      Session::destroy();
    }

   ?>
  <div class="social">

  <?php
    $login = Session::get("usrlogin");
    if($login == false){ ?>
       <a href="login.php" class="loginBtn">Login</a> 
    <?php }else { ?>
      <a href="myaccount.php" class="loginBtn">My Account</a> 
  <?php } ?>
  <a href="postyouradd.php" title="" class="loginBtn">Post Your Ad</a>

  </div>
</div>
            
      

<!-- +++++++++++++++++++++++++++++++++++++++++++++++
   MENU PART
++++++++++++++++++++++++++++++++++++++++++++++++++++-->


  <div class="menu">
    <ul>
      <li><a href="index.php">Home</a></li>
      <li><a href="services.php">Services</a></li>
      <li><a href="allad.php">All Ad</a></li>
	  
	  



     <!--  <li class="dropdown">
        <a href="#" class="dropbtn">All Ad ▼</a>
          <div class="dropdown-content">
            <a href="#">Economy Flat</a>
            <a href="#">Standard Flat</a>
            <a href="#">Luxury Flat</a> 
          </div>
      </li> 
 -->
      <li><a href="about.php">About</a></li>
      <li><a href="contact.php">Contact</a></li>
    </ul>
  </div>




