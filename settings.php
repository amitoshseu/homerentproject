<?php include 'inc/header.php';?>
<link href="css/styles2.css" rel="stylesheet" type="text/css" media="all"/>
<?php
    $login = Session::get("usrlogin");
    if($login == false){
        header("Location:login.php");
    }
 ?>

  <?php 
     $usrId = Session::get("usrId");
     if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit1'])) {
         $updateUsr = $usr->usersUpdate($_POST,$usrId);
     }
 ?>

  <!-- FOR SUBMIT TWO BUTTON -->
  <?php 
     $usrId = Session::get("usrId");
     if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit2'])) {
         $updatePass = $usr->passwordUpdate($_POST,$usrId);
     }
 ?>


<br><br>
<div class="settingSidebar">
	<br><br><br>
	&#8618;<a href="myaccount.php" title="">My Account</a><br><br>
	&#8618;<a href="settings.php" title="">Settings</a>
</div>


<div class="settingMainbar">
 <div class="settingDiv">
 <?php 
     $id = Session::get("usrId");
     $getdata = $usr->getUserData($id);
     if($getdata){
         while ($result = $getdata->fetch_assoc()){
 
  ?>

<h1 class="settingTitle">Settings</h1><hr>
<p class="">Update Information</p><br>


<form method="post">
<?php 
    if (isset($updateUsr)) {
      echo $updateUsr;
    }

 ?>
 <?php 
     if (isset($updatePass)) {
       echo $updatePass;
     }

  ?><br><br>
    <b>Email : <?php echo $result['email']; ?></b><br><br>
	<label for="name">Name</label><br>
	<input type="text" name="name" value="<?php echo $result['name']; ?>"><br>

	<label for="phone">Phone</label><br>
	<input type="text" name="phone" value="<?php echo $result['phone']; ?>"><br>

	<label for="address">Address</label><br>
	<input type="text" name="address" value="<?php echo $result['address']; ?>"><br>
	<input type="submit" name="submit1" value="Update Information">
</form>
<?php } } ?>

<br><br><br><br>
<form method="post">
	<p class="">Update Password</p><hr><br>

	<label for="">New Password</label><br>
	<input type="password" class="pass" name="newPass" required placeholder=""><br>

	<label for="">Confirm New Password</label><br>
	<input type="password" name="conPass"  class="pass" required placeholder=""><br>

	<input type="submit" name="submit2" value="Update Password">
</form>
<br><hr>
<br><br><br>
<div align="center">
		<div class="logout">
 			<a href="?cid=<?php Session::get('usrId') ?>">Logout</a>
		</div>
</div>


	</div>
</div>



<br><br>
<?php include 'inc/footer.php';?>
