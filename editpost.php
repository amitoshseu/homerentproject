<?php include 'inc/header.php';?>
<link href="css/styles2.css" rel="stylesheet" type="text/css" media="all"/>
<br><br>
<?php
    $login = Session::get("usrlogin");
    if($login == false){
        header("Location:login.php");
    }
 ?>

 <?php 
     if (!isset($_GET['postid']) || $_GET['postid'] == NULL) {
         echo "<script>window.location='myaccount.php';</script>";
     }else{
        $id = preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['postid']);
     }

    
     if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
         $updatePost = $add->updateYourPost($_POST,$_FILES,$id);
     } 


 ?> 

<div style="width: 70%;"  class="offerForRentDiv">
<div class="allAdDivTitle">Update Your Post</div><hr>
	<?php
	   if (isset ($updatePost)){
	       echo $updatePost;

	   }
	 ?>   

      <?php 
	    $getPost = $add->getPostById($id);
	    if ($getPost) {
	        while ($result = $getPost->fetch_assoc()) {
		?>
	<form method="post" enctype="multipart/form-data">
		<b>Previous Category: <label for="category"><?php echo $result['category'];?></label></b>
		 <select  name="category" required >
		  <option value="" selected disabled>Select Flat Category</option>
		  <option value="Economy">Economy</option>
		  <option value="Standard">Standard</option>
		  <option value="Luxary">Luxary</option>
		</select><br>
        <b>Previous Location: <label for="location"><?php echo $result['location'];?></label></b>
		 <select name="location" required >
		 <option  value="" selected disabled>Select Location</option>
		  <option value="Adabor">Adabor</option>
		  <option value="Azimpur">Azimpur</option>
		  <option value="Badda">Badda</option>
		  <option value="Banani">Banani</option>
		  <option value="Banani DOHS">Banani DOHS</option>
		  <option value="Banglamotor">Banglamotor</option>
		  <option value="Bangshal">Bangshal</option>
		  <option value="Baridhara">Baridhara</option>
		  <option value="Basabo">Basabo</option>
		  <option value="Basundhara">Basundhara</option>
		  <option value="Cantonment">Cantonment</option>
		  <option value="Chaukbazar">Chaukbazar</option>
		  <option value="Demra">Demra</option>
		  <option value="Dhamrai">Dhamrai</option>
		  <option value="Dhanmondi">Dhanmondi</option>
		  <option value="Dohar">Dohar</option>
		  <option value="Elephant Road">Elephant Road</option>
		  <option value="Gulshan">Gulshan</option>
		  <option value="Hazaribagh">Hazaribagh</option>
		  <option value="Jatrabari">Jatrabari</option>
		  <option value="Kafrul">Kafrul</option>
		  <option value="Kamrangirchar">Kamrangirchar</option>
		  <option value="Kawranbazar">Kawranbazar</option>
		  <option value="Keraniganj">Keraniganj</option>
		  <option value="Khilgoan">Khilgoan</option>
		  <option value="Khilkhet">Khilkhet</option>
		  <option value="Kotowali">Kotowali</option>
		  <option value="Lalbag">Lalbag</option>
		  <option value="Malibag">Malibag</option>
		  <option value="Mirpur">Mirpur</option>
		  <option value="Mirpur DOHS">Mirpur DOHS</option>
		  <option value="Mogbazar">Mogbazar</option>
		  <option value="Mohakhali">Mohakhali</option>
		  <option value="Mohakhali DOHS">Mohakhali DOHS</option>
		  <option value="Mohammadpur">Mohammadpur</option>
		  <option value="Motijheel">Motijheel</option>
		  <option value="Nawabganj">Nawabganj</option>
		  <option value="New Market">New Market</option>
		  <option value="Paltan">Paltan</option>
		  <option value="Purbachal">Purbachal</option>
		  <option value="Ramna">Ramna</option>
		  <option value="Rampura">Rampura</option>
		  <option value="Savar">Savar</option>
		  <option value="Saydabad">Saydabad</option>
		  <option value="Shajahanpur">Shajahanpur</option>
		  <option value="Sutrapur">Sutrapur</option>
		  <option value="Tejgaon">Tejgaon</option>
		  <option value="Tongi">Tongi</option>
		  <option value="Uttara">Uttara</option>
		  <option value="Wari">Wari</option>
		</select>
       <input type="text" name="caption" value="<?php echo $result['caption'];?>" maxlength="60" required placeholder="Enter your ad caption <60 Word"><br>
		
		<p>Add Photo</p>
		<img src="<?php echo $result['image']; ?>"  height="80px" width="200px"><br/>
		<input type="file" required class="chooseFile" name="image">

		<br><br>
		<div class="allAdDivTitle">Fill in the details</div>
		<hr>
		 <b>Previous Month: <label for="month"><?php echo $result['month'];?></label></b>
		 <select class="" name="month" required>
		  <option  value="" selected disabled>For The Month Of</option>
		  <option value="January">January</option>
		  <option value="February">February</option>
		  <option value="March">March</option>
		  <option value="April">April</option>
		  <option value="May">May</option>
		  <option value="June">June</option>
		  <option value="July">July</option>
		  <option value="August">August</option>
		  <option value="September">September</option>
		  <option value="October">October</option>
		  <option value="November">November</option>
		  <option value="December">December</option>
		</select><br>
		<b>Previous Beds: <label for="bed"><?php echo $result['bed'];?></label></b>  
		<select class="" name="bed" required>
		  <option  value="" selected disabled>Beds</option>
		  <option value="1">One</option>
		  <option value="2">Two</option>
		  <option value="3">Three</option>
		  <option value="4">Four</option>
		  <option value="5">Five</option>
		</select><br>
		<b>Previous Baths: <label for="bath"><?php echo $result['bath'];?></label></b>
		 <select class="" name="bath" required>
		  <option  value="" selected disabled>Bath</option>
		  <option value="1">One</option>
		  <option value="2">Two</option>
		  <option value="3">Three</option>
		  <option value="4">Four</option>
		</select><br>
		
		<input type="text" name="size" value="<?php echo $result['size'];?>" required placeholder="Size(sqft)"><br>
		<input type="text" name="address" value="<?php echo $result['address'];?>" required placeholder="Please enter your address"><br>
		<textarea id="description" name="body" required placeholder="Write something.... More Details= more response" style="height:200px"><?php echo $result['body'];?>
		</textarea>
		<input type="text" class="" name="rate" value="<?php echo $result['rate'];?>"required placeholder="Rent (Tk) /month">
        <input type="text" name="phone" value="<?php echo $result['phone'];?>" required placeholder="Enter your active phone number"><br>
     <br><br>
     <div class="allAdDivTitle">Contact Details</div><hr>

 	 <?php
 	     $id = Session::get("usrId");
 	     $getdata = $usr->getUserData($id);
 	     if($getdata){
 	         while ($result = $getdata->fetch_assoc()){
 	    ?>
 	    
     <p><?php echo $result['name']; ?> </p><br>
     <p><?php echo $result['email']; ?></p><br>
     <?php } }?>

     <input type="submit" name="submit" value="Update">
		 
	</form>
	<?php } } ?>

</div>

<?php include 'inc/footer.php';?>