-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2018 at 10:41 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.1.15-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homeRentUpdate`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addPost`
--

CREATE TABLE `tbl_addPost` (
  `postId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `category` varchar(60) NOT NULL,
  `location` varchar(100) NOT NULL,
  `caption` varchar(150) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bed` int(11) NOT NULL,
  `bath` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `rate` varchar(6) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `month` varchar(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_addPost`
--

INSERT INTO `tbl_addPost` (`postId`, `userId`, `category`, `location`, `caption`, `image`, `bed`, `bath`, `size`, `address`, `body`, `rate`, `phone`, `month`, `date`, `type`) VALUES
(5, 8, 'Economy', 'Rampura', 'house for rent', 'admin/uploads/9c30ddfdf1.jpg', 3, 1, 200, 'rampura', 'this is a nice house', '15000', '01636502457', 'March', '2017-12-12 06:58:41', 0),
(6, 8, 'Standard', 'Banani', 'rental house', 'admin/uploads/075dacc067.jpg', 2, 2, 150, 'banani', 'nice house', '25000', '01636502457', 'April', '2017-12-20 06:07:41', 0),
(7, 9, 'Standard', 'Gulshan', 'for rent', 'admin/uploads/9682d501d1.jpg', 3, 2, 200, 'gulshan', 'this is a nice house', '25000', '01917789924', 'February', '2017-12-20 06:12:08', 0),
(8, 9, 'Standard', 'Banani', 'its looking very nice', 'admin/uploads/b57b70d445.png', 2, 2, 250, 'banani', 'its a very comfortable house', '25000', '01917789924', 'March', '2017-12-20 06:16:00', 0),
(9, 11, 'Luxary', 'Azimpur', 'RENT FOR HOUSE', 'admin/uploads/0be701ce35.jpg', 3, 2, 250, 'Gazipur', 'A house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containing plumbing, ventilation and electrical systems', '30000', '01636502459', 'March', '2018-01-06 19:37:49', 0),
(10, 11, 'Standard', 'Banani', 'THIS IS A NICE HOUSE', 'admin/uploads/8a85a7591b.jpg', 2, 2, 300, 'gulshan', 'A house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containing plumbing, ventilation and A house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containing plumbing, ventilation and electrical systemselectrical systems', '25000', '01636502459', 'August', '2018-01-06 19:42:58', 0),
(11, 11, 'Economy', 'Gulshan', 'NICE FLAT FOR RENT', 'admin/uploads/be7b9675bc.jpg', 4, 3, 200, 'muktagacha', 'A house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containiA house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containing plumbing, ventilation and electrical systemsng plumbing, ventilation and electrical systems', '30000', '01636502459', 'October', '2018-01-06 19:44:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `adminId` int(11) NOT NULL,
  `adminName` varchar(255) NOT NULL,
  `adminUser` varchar(255) NOT NULL,
  `adminEmail` varchar(255) NOT NULL,
  `adminPass` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`adminId`, `adminName`, `adminUser`, `adminEmail`, `adminPass`) VALUES
(6, 'imran', 'imran', 'imran@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(7, 'arif rabbani', 'arif', 'arif.rabbani@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(8, 'Amitosh Gain', 'amitoshseu', 'amitosh.seu@gmail.com', '35377d9c9721619636f55e661d1df077');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `company` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `pass` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `phone`, `address`, `pass`) VALUES
(3, 'Amitosh Gain.', 'amitosh.seu@gmail.com', '01763356347', 'Kunjbon, Rampura.', '202cb962ac59075b964b07152d234b70'),
(4, 'Arif Rabbani', 'arif.rabbani@gmail.com', '01763356347', 'Gazipur', '827ccb0eea8a706c4c34a16891f84e7b'),
(5, 'Ajoy Devgan', 'ajoy@gmail.com', '01515259328', 'Uttara, Dhaka', '202cb962ac59075b964b07152d234b70'),
(6, 'Jane Doe', 'janedoe@example.com', '01763356347', 'Rampura', '35377d9c9721619636f55e661d1df077'),
(7, 'Jane Doe', 'jane@example.com', '01763356347', 'Rampura', '202cb962ac59075b964b07152d234b70'),
(8, 'bhuan rony', 'rony@gmail.com', '01641000100', 'nikonjo', '1e191d851b3b49a248f4ea62f6b06410'),
(9, 'arif rabbani', 'arif@gmail.com', '01917789924', 'Gazipur', 'e10adc3949ba59abbe56e057f20f883e'),
(10, 'rabbani', 'arif.rabbani619@gmail.com', '01636502457', 'paratongi', '9c7cc2cde1939666d314378b18857721'),
(11, 'habib', 'habib@gmail.com', '01636502459', 'muktagacha', '652d3266220e0aacb047d3aa6f51f1b0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_addPost`
--
ALTER TABLE `tbl_addPost`
  ADD PRIMARY KEY (`postId`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_addPost`
--
ALTER TABLE `tbl_addPost`
  MODIFY `postId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
