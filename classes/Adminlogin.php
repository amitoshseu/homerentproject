<?php 
	$filepath = realpath(dirname(__FILE__));
	include_once ($filepath.'/../lib/Session.php');
	Session::checkLogin();
	include_once ($filepath.'/../lib/Database.php');
	include_once ($filepath.'/../helpers/Format.php');
   
?>

 
<?php 
	class Adminlogin
	{
		private $db;
		private $fm;
		public function __construct()
		{
			$this->db = new Database();
			$this->fm = new Format();
		}
		public function adminLogin($adminUser,$adminPass){
			$adminUser= $this->fm->validation($adminUser);
			$adminPass= $this->fm->validation($adminPass);

			$adminUser = mysqli_real_escape_string($this->db->link,$adminUser);
			$adminPass = mysqli_real_escape_string($this->db->link,$adminPass);

			if(empty($adminUser) || empty($adminPass)){
				$loginmsg="Username or Password must not be empty ! ";
				return $loginmsg;
			} else{
				$query = "SELECT * FROM tbl_admin WHERE adminUser = '$adminUser' AND adminPass = '$adminPass'";
				$result = $this->db->select($query);
				if($result != false){
					$value = $result->fetch_assoc();
					Session::set("adminlogin",true);
					Session::set("adminId",$value['adminId']);
					Session::set("adminUser",$value['adminUser']);
					Session::set("adminName",$value['adminName']);

					header("Location:dashbord.php");
				}else{
					$loginmsg="Username or Password not match ! ";
					return $loginmsg;
				}
			}
		}

			public function adminRegistration($data){
				$adminName = mysqli_real_escape_string($this->db->link,$data['adminName']);
				$adminUser = mysqli_real_escape_string($this->db->link,$data['adminUser']);
				$adminEmail = mysqli_real_escape_string($this->db->link,$data['adminEmail']);
				$adminPass = mysqli_real_escape_string($this->db->link, md5($data['adminPass']));
				$conPass = mysqli_real_escape_string($this->db->link,md5($data['conPass']));

				if($adminName == "" ||  $adminUser =="" || $adminEmail =="" || $adminPass =="" || $conPass =="" ){
		    	$msg="<span class='error'>Field must not be empty!</span>";
				return $msg;
				}

				if($adminPass != $conPass){
					$msg="<span class='error'>Password doesn't match!</span>";
					return $msg;
				}


				$mailquery = "SELECT * FROM tbl_admin WHERE adminEmail='$adminEmail' LIMIT 1";
				$mailchk   = $this->db->select($mailquery);
				if ($mailchk != false) {
					$msg="<span class='error'>Email already Exist!</span>";
					return $msg;
				}elseif (!filter_var($adminEmail, FILTER_VALIDATE_EMAIL)) {
				    $msg="<span class='error'>Please enter a valid email!</span>";
					return $msg;

				}else{
					$query = "INSERT INTO tbl_admin(adminName,adminUser,adminEmail,adminPass) VALUES('$adminName','$adminUser','$adminEmail','$adminPass')";

			    $inserted_row = $this->db->insert($query);
				if($inserted_row){
					$msg = "<span class='success'>Registraion Completed Successfully</span>";
					return $msg;
				}else{
					$msg = "<span class='error'>Registraion Failed</span>";
					return $msg;
		
				}
				}

			}

	 
		
	}
 ?>


