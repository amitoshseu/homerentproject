<?php 
	$filepath = realpath(dirname(__FILE__));
	include_once ($filepath.'/../lib/Database.php');
	include_once ($filepath.'/../helpers/Format.php');
?>
    
 
<?php 
	/**
	* 
	*/
	class Users{
		private $db;
		private $fm;

		public function __construct()
		{
			$this->db = new Database();
			$this->fm = new Format();
		}

		public function usersRegistration($data){
			$name = mysqli_real_escape_string($this->db->link,$data['name']);
			$email = mysqli_real_escape_string($this->db->link,$data['email']);
			$phone = mysqli_real_escape_string($this->db->link,$data['phone']);
			$address = mysqli_real_escape_string($this->db->link,$data['address']);
			$pass = mysqli_real_escape_string($this->db->link, md5($data['pass']));
			$conPass = mysqli_real_escape_string($this->db->link,md5($data['conPass']));

			if($name == "" ||  $email =="" || $phone =="" || $address =="" || $pass =="" || $conPass =="" ){
	    	$msg="<span class='error'>Field must not be empty!</span>";
			return $msg;
			}

			if($pass != $conPass){
				$msg="<span class='error'>Password doesn't match!</span>";
				return $msg;
			}

			elseif(!preg_match('/^[0-1]{2}[0-9]{9}$/',$phone)){
							$msg="<span class='error'>Please enter a valid phone number!</span>";
							return $msg;
						}

			$mailquery = "SELECT * FROM tbl_users WHERE email='$email' LIMIT 1";
			$mailchk   = $this->db->select($mailquery);
			if ($mailchk != false) {
				$msg="<span class='error'>Email already Exist!</span>";
				return $msg;
			}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    $msg="<span class='error'>Please enter a valid email!</span>";
				return $msg;

			}else{
				$query = "INSERT INTO tbl_users(name,email,phone,address,pass) VALUES('$name','$email','$phone','$address','$pass')";


		    $inserted_row = $this->db->insert($query);
			if($inserted_row){
				$msg = "<span class='success'>Registraion Completed Successfully</span>";
				return $msg;
			}else{
				$msg = "<span class='error'>Registraion Failed</span>";
				return $msg;
	
			}
			}

		}



		public function usersLogin($data){ //u can choose any name instead of $data
			$email = mysqli_real_escape_string($this->db->link,$data['email']);
			$pass  = mysqli_real_escape_string($this->db->link, md5($data['pass']));
			if (empty($email) || empty($pass)) {
				$msg="<span class='error'>Field must not be empty!</span>";
				return $msg;
			}
			$query = "SELECT * FROM tbl_users WHERE email='$email' AND pass = '$pass'";
			$result = $this->db->select($query);
			if ($result != false) {
				$value = $result->fetch_assoc();
				Session::set("usrlogin",true);
				Session::set("usrId",$value['id']);
				Session::set("usrName",$value['name']);
				header("Location:postyouradd.php");
			}else{
				$msg="<span class='error'>Email or Password Not match!</span>";
				return $msg;
			}

		}


		public function getUserData($id){
			$query = "SELECT * FROM tbl_users WHERE id='$id'";
			$result = $this->db->select($query);
			return $result;
		}


		public function usersUpdate($data,$usrId){
				$name = mysqli_real_escape_string($this->db->link,$data['name']);
				$phone = mysqli_real_escape_string($this->db->link,$data['phone']);
				$address = mysqli_real_escape_string($this->db->link,$data['address']);

				if($name == "" || $phone == "" ||  $address ==""){
		    	$msg="<span class='error'>Field must not be empty!</span>";
				return $msg;
				}else{
			
					$query = "UPDATE tbl_users
					SET
					name 		= '$name',
					phone 		= '$phone',
					address 	= '$address'
					WHERE id 	= '$usrId'";

					$updated_row = $this->db->update($query);

					if($updated_row){
						$msg = "<span class='success'>Users Details Updated Successfully</span>";
						return $msg;
					}else{
						$msg="<span class='error'>Users Deails Not Updated!</span>";
						return $msg;
					}
				}
		}

		public function passwordUpdate($data,$usrId){
				$newPass = mysqli_real_escape_string($this->db->link, md5($data['newPass']));
				$conPass = mysqli_real_escape_string($this->db->link, md5($data['conPass']));

				if($newPass == "" || $conPass == ""){
		    	$msg="<span class='error'>Field must not be empty!</span>";
				return $msg;
				}
				if($newPass != $conPass){
					$msg="<span class='error'>Password doesn't match!</span>";
					return $msg;
				}

				else{
			
					$query = "UPDATE tbl_users
					set
					pass 		= '$conPass'
					WHERE id 	= '$usrId'";

					$updated_row = $this->db->update($query);

					if($updated_row){
						$msg = "<span class='success'>Password Updated Successfully</span>";
						return $msg;
					}else{
						$msg="<span class='error'>Password Updated Failed!</span>";
						return $msg;
					}
				}
		}

		public function getUserDataInAdmin($id){
			$query = "SELECT * FROM tbl_users WHERE id='$id'";
			$result = $this->db->select($query);
			return $result;
		}

    public function getAllUser(){
       $query = "SELECT * FROM tbl_users ORDER BY id DESC";
       $result = $this->db->select($query);
       return $result;
    }

    // public function delUserById(){
    //    $query = "SELECT * FROM tbl_users ORDER BY id DESC";
    //    $result = $this->db->select($query);
    //    return $result;
    // }
     


	}


 ?>