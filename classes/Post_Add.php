<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../helpers/Format.php');
?>
<?php
class Post_Add{
	private $db;
	private $fm;

	public function __construct()
    {
  			$this->db = new Database();
  			$this->fm = new Format();
    }

   public function postYourAdd($data,$file,$userId){
      $category   = mysqli_real_escape_string($this->db->link,$data['category']);
      $location   = mysqli_real_escape_string($this->db->link,$data['location']);
      $caption    = mysqli_real_escape_string($this->db->link,$data['caption']);
      $bed        = mysqli_real_escape_string($this->db->link,$data['bed']);
      $bath       = mysqli_real_escape_string($this->db->link,$data['bath']);
      $size       = mysqli_real_escape_string($this->db->link,$data['size']);
      $address    = mysqli_real_escape_string($this->db->link,$data['address']);
      $body       = mysqli_real_escape_string($this->db->link,$data['body']);
      $rate       = mysqli_real_escape_string($this->db->link,$data['rate']);
      $phone       = mysqli_real_escape_string($this->db->link,$data['phone']);
      $month       = mysqli_real_escape_string($this->db->link,$data['month']);

      $permited  = array('jpg', 'jpeg', 'png', 'gif');
      $file_name = $file['image']['name'];
      $file_size = $file['image']['size'];
      $file_temp = $file['image']['tmp_name'];


      $div = explode('.', $file_name);
      $file_ext = strtolower(end($div));
      $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
      $uploaded_image = "admin/uploads/".$unique_image;
      if ($category == "" || $location == "" || $caption == "" || $bed == "" || $bath == "" || $size == "" || $address == "" || $body == "" || $rate == "" || $phone == "" || $month =="" ) {
        $msg="<span class='error'>Fields must not be empty !</span>";
        return $msg;
      
      }elseif ($file_size >1048567) {
     echo "<span class='error'>Image Size should be less then 1MB!
     </span>";
    
    } elseif (in_array($file_ext, $permited) === false) {
      echo "<span class='error'>You can upload only:-".implode(', ', $permited)."</span>";
    
    }else {
          move_uploaded_file($file_temp, $uploaded_image);
          $query ="INSERT INTO tbl_addPost(userId, category, location, caption, image, bed ,bath, size, address, body, rate, phone, month) VALUES('$userId','$category','$location','$caption','$uploaded_image','$bed','$bath','$size','$address','$body','$rate','$phone','$month') ";
          $Insert_row = $this->db->insert($query);
        if($Insert_row){
           $msg ="<span class='success'>Your Ad Successfully Posted.</span>";
        return $msg;
          
      } else {
             $msg ="<span class='error'>Ad Posted Failed.</span>";
             return $msg;
          }
      }
   }


   public function updateYourPost($data,$file,$id){
      $category   = mysqli_real_escape_string($this->db->link,$data['category']);
      $location   = mysqli_real_escape_string($this->db->link,$data['location']);
      $caption    = mysqli_real_escape_string($this->db->link,$data['caption']);
      $bed        = mysqli_real_escape_string($this->db->link,$data['bed']);
      $bath       = mysqli_real_escape_string($this->db->link,$data['bath']);
      $size       = mysqli_real_escape_string($this->db->link,$data['size']);
      $address    = mysqli_real_escape_string($this->db->link,$data['address']);
      $body       = mysqli_real_escape_string($this->db->link,$data['body']);
      $rate       = mysqli_real_escape_string($this->db->link,$data['rate']);
      $phone       = mysqli_real_escape_string($this->db->link,$data['phone']);
      $month       = mysqli_real_escape_string($this->db->link,$data['month']);

      $permited  = array('jpg', 'jpeg', 'png', 'gif');
      $file_name = $file['image']['name'];
      $file_size = $file['image']['size'];
      $file_temp = $file['image']['tmp_name'];


      $div = explode('.', $file_name);
      $file_ext = strtolower(end($div));
      $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
      $uploaded_image = "admin/uploads/".$unique_image;
      if ($category == "" || $location == "" || $caption == "" || $bed == "" || $bath == "" || $size == "" || $address == "" || $body == "" || $rate == "" || $phone == "" || $month =="" ) {
      	$msg="<span class='error'>Fields must not be empty !</span>";
				return $msg;
      }else{
       if (!empty($file_name)) {
        
          if ($file_size >1048567) {
             echo "<span class='error'>Image Size should be less then 1MB!
             </span>";
    
          } elseif (in_array($file_ext, $permited) === false) {
            echo "<span class='error'>You can upload only:-".implode(', ', $permited)."</span>";
    
          }else {
      	  move_uploaded_file($file_temp, $uploaded_image);
      	  $query = "UPDATE tbl_addPost
            SET
            category = '$category', 
            location = '$location', 
            caption = '$caption', 
            image = '$uploaded_image', 
            bed = '$bed', 
            bath = '$bath', 
            size = '$size', 
            size = '$size', 
            address = '$address', 
            body = '$body', 
            rate= '$rate', 
            phone= '$phone', 
            month= '$month'
            WHERE postId = '$id'";
           
          $updated_row = $this->db->update($query);
          if($updated_row){
            $msg = "<span class='success'>Post Update Successfully</span>";
            return $msg;
          }else{
            $msg = "<span class='error'>Post Not Updated </span>";
            return $msg;
      
            }
          }
          }else{
                $query = "UPDATE tbl_addPost
                  SET
                  category = '$category', 
                  location = '$location', 
                  caption = '$caption', 
                  bed = '$bed', 
                  bath = '$bath', 
                  size = '$size', 
                  size = '$size', 
                  address = '$address', 
                  body = '$body', 
                  rate= '$rate', 
                  phone= '$phone', 
                  month= '$month'
                  WHERE postId = '$id'";

                  $updated_row = $this->db->update($query);
                  if($updated_row){
                    $msg = "<span class='success'>Post Update Successfully</span>";
                    return $msg;
                  }else{
                    $msg = "<span class='error'>Post Not Updated </span>";
                    return $msg;
                  
              }
          }
      }
   }


    public function getAllAd(){
      $query = "SELECT * FROM tbl_addPost WHERE type = '0' ORDER BY date DESC";
      $result = $this->db->select($query);
    return $result;
    }
   
    public function getSingleAd($id){
      $query = "SELECT * FROM tbl_addPost WHERE postId = '$id'";
      $result = $this->db->select($query);
      return $result;
    }

    public function getEmailAddress($id){
      $query = "SELECT tbl_users.email, tbl_addPost.postId FROM tbl_users INNER JOIN tbl_addPost ON tbl_users.id = tbl_addPost.userId  AND postId='$id'";
      $result = $this->db->select($query);
      return $result;
    }

    public function getAdHistory($usrId){
      $query = "SELECT * FROM tbl_addPost WHERE userId = '$usrId' ORDER BY date DESC";
      $result = $this->db->select($query);
      return $result;
    }

    public function addRemoveFromAllAd($id,$time){
      
      $id = mysqli_real_escape_string($this->db->link,$id);
      $time = mysqli_real_escape_string($this->db->link,$time);
      $query = "UPDATE tbl_addPost
        SET
        type = '1'
        WHERE userId = '$id' AND date = '$time'";
        $updated_row = $this->db->update($query);
        if($updated_row){
          $msg = "<span class='success'>Post Successfully Removed</span>";
          return $msg;
        }else{
          $msg="<span class='error'>Post Not Removed!</span>";
          return $msg;
        }
}

/*
  public function getFilterAd($search){
      $search = mysqli_real_escape_string($this->db->link,$search);
     

      $query = "SELECT * FROM tbl_addPost WHERE location = '$search'";
      $result = $this->db->select($query);
      return $result;
  }
  */


   public function getPostById($id){
      $query = "SELECT * FROM tbl_addPost WHERE postId = '$id'";
      $result = $this->db->select($query);
      return $result;
   }

   public function getAllAdInAdmin(){
      $query = "SELECT * FROM tbl_addPost ORDER BY date DESC";
      $result = $this->db->select($query);
      return $result;
   }
   
    public function delpostaddById($id){ 
    // $postId = mysqli_real_escape_string($this->db->link,$id);
    $query  = "DELETE FROM tbl_addPost WHERE postId = '$id'";
    $result = $this->db->delete($query);
    return $result;
    }     

    /*public function getFirstHalfAddress(){
      $query = "SELECT * FROM tbl_addPost ORDER BY location";
      $result = $this->db->select($query);
      return $result;
    }*/

 }
