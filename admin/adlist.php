<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Post_Add.php'; ?> 
<?php include_once '../helpers/Format.php';?>

<?php 
	 $add  = new Post_Add();
	 $fm   = new Format();
 ?>

<?php 
	if (isset($_GET['delpostadd'])) {
		$id = preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['delpostadd']);
		$delpostadd = $add->delpostaddById($id);
	}
 ?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>All Ad List</h2>
        <div class="block">  
        	<?php 
                    if (isset($delpostadd)) {
                    	echo $delpostadd;
                    }

            ?>
                 

            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>SL</th>
					<th>User Details</th>
					<th>Category</th>
					<th>Location</th>
					<th>Caption</th>
					<th>Size</th>
				 	<th>Bed</th>
				 	<th>Bath</th>
				 	<th>Rate</th>
					<th>Month</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$getAdd = $add->getAllAdInAdmin();
					$i=0; 
					if ($getAdd) {
						while ($result = $getAdd->fetch_assoc()) {
							$i++;
					
				 ?>
				<tr class="odd gradeX">
					    <td><?php echo $i; ?></td>
					    <td><a href="customer.php?customerId=<?php echo $result['userId']; ?>" title="View Details">User Details</a></td>
					    <td><?php echo $result['category']; ?></td>
					    <td><?php echo $result['location']; ?></td>
					    <td><?php echo $result['caption']; ?></td>
					    <td><?php echo $result['size']; ?></td>
					    <td><?php echo $result['bed']; ?></td>
					    <td><?php echo $result['bath']; ?></td>
					    <td><?php echo $result['rate']; ?></td>
					    <td><?php echo $result['month']; ?></td>
				
						<td><a onclick="return confirm('Are you sure you want to delete this item?')" 
						href="?delpostadd=<?php echo $result['postId']; ?>">Delete</a></td>
						
				</tr>
				<?php } } ?>
				
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
