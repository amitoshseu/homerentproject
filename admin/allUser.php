<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Users.php'; ?> 
<?php include_once '../helpers/Format.php';?>

<?php 
	 $usr  = new Users();
	 $fm   = new Format();
 ?>

<?php 
	if (isset($_GET['delUsr'])) {
		$id = preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['delUsr']);
		$delUsr = $usr->delUserById($id);
	}
 ?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>All Ad List</h2>
        <div class="block">  
        	<?php 
                    if (isset($delUsr)) {
                    	echo $delUsr;
                    }

            ?>
                 

            <table class="data display datatable" id="example">
			<thead>
				<tr>
					<th>SL</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Address</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$getUsr = $usr->getAllUser();
					$i=0; 
					if ($getUsr) {
						while ($result = $getUsr->fetch_assoc()) {
							$i++;
					
				 ?>
				<tr class="odd gradeX">
					    <td><?php echo $i; ?></td>
					    <td><?php echo $result['name']; ?></td>
					    <td><?php echo $result['email']; ?></td>
					    <td><?php echo $result['phone']; ?></td>
					    <td><?php echo $result['address']; ?></td>
				
						
				</tr>
				<?php } } ?>
				
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
	